package android.util;

public class Log {
    static public int d(String tag, String msg) {
        System.out.printf("DEBUG: %s: %s\n", tag, msg);
        return 0;
    }

    static public int i(String tag, String msg) {
        System.out.printf("INFO: %s: %s\n", tag, msg);
        return 0;
    }

    static public int e(String tag, String msg) {
        System.out.printf("ERROR: %s: %s\n", tag, msg);
        return 0;
    }

    static public int w(String tag, String msg) {
        System.out.printf("WARNING %s: %s\n", tag, msg);
        return 0;
    }
}
