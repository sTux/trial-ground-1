package com.stux.todo.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.stux.todo.CoroutineDispatcherRule
import com.stux.todo.database.TodoEntity
import com.stux.todo.getOrAwait
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class TodoViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineDispatcherRule = CoroutineDispatcherRule()

    private lateinit var repository: TodoRepositoryMock
    private lateinit var viewModel: TodoViewModel

    @Before
    fun setUp() {
        repository = TodoRepositoryMock()
        viewModel = TodoViewModel(repository)

        runBlockingTest {
            repository.insert(
                TodoEntity(null, "Hello World", null, false)
            )
        }
    }

    @Test
    fun `Test task creation and insertion`() {
        assertThat(repository.tasks).containsExactly(
            TodoEntity(id = repository.lastIndex,
                subject = "Hello World",
                description = null,
                is_complete = false)
        )
    }

    @Test
    fun `Test for unfinished tasks in db`() {
        val tasks = viewModel.unfinishedTasks.getOrAwait()

        assertThat(tasks).containsExactly(
            TodoEntity(id = repository.lastIndex,
                subject = "Hello World",
                description = null,
                is_complete = false)
        )
    }

    @Test
    fun `Test for finished tasks in db`() {
        // First manually setting the task as complete
        repository.tasks[0].is_complete = true
        val tasks = viewModel.finishedTasks.getOrAwait()

        assertThat(tasks).containsExactly(
            TodoEntity(id = repository.lastIndex,
                subject = "Hello World",
                description = null,
                is_complete = true)
        )
    }

    @Test
    fun `Test marking a task as done`() {
        viewModel.markFinished(repository.lastIndex)
        val tasks = viewModel.finishedTasks.getOrAwait()

        assertThat(tasks).containsExactly(
            TodoEntity(id = 0, subject = "Hello World", description = null, is_complete = true)
        )
    }

    @Test
    fun `Test successful deleting of a task)`() {
        viewModel.deleteTask(repository.lastIndex)
        assertThat(repository.tasks).isEmpty()
    }
}