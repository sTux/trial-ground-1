package com.stux.todo.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.stux.todo.CoroutineDispatcherRule
import com.stux.todo.database.TodoEntity
import com.stux.todo.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class TaskAddViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()


    @get:Rule
    val coroutineDispatcherRule = CoroutineDispatcherRule()

    private lateinit var repository: TodoRepositoryMock
    private lateinit var viewModel: TaskAddViewModel


    @Before
    fun setUp() {
        repository = TodoRepositoryMock()
        viewModel = TaskAddViewModel(repository)
    }

    @Test
    fun `Test the task validator function identifies large title`() {
        val word = buildString {
            for (i in 1..Constants.MAXIMUM_TITLE_LENGTH + 1) this.append("i")
        }

        val entity = TodoEntity(null, word, word, false)
        assertThat(viewModel.validateTasks(entity)).isFalse()
    }

    @Test
    fun `Test the task validator function identifies empty titles`() {
        val entity = TodoEntity(null, "", "", false)
        assertThat(viewModel.validateTasks(entity)).isFalse()
    }

    @Test
    fun `Test that task validator function identifies valid title`() {
        val title = buildString {
            for (i in 1..Constants.MAXIMUM_TITLE_LENGTH) this.append("i")
        }

        val entity = TodoEntity(null, title, title, false)
        assertThat(viewModel.validateTasks(entity)).isTrue()
    }

    @Test
    fun `Test task insertion with invalid data`() {
        val word = buildString {
            for (i in 1..Constants.MAXIMUM_TITLE_LENGTH + 1) this.append("i")
        }

        val entity = TodoEntity(null, word, word, false)
        viewModel.insertTask(entity)
        val result = viewModel.status.value

        assertThat(result).isEqualTo(-1)
    }

    @Test
    fun `Test task insertion with empty title`() {
        val entity = TodoEntity(null, "", "word", false)
        viewModel.insertTask(entity)
        val result = viewModel.status.value

        assertThat(result).isEqualTo(-1)
    }

    @Test
    fun `Test task insertion with valid data`() {
        val title = buildString {
            for (i in 1..Constants.MAXIMUM_TITLE_LENGTH) this.append("i")
        }

        val entity = TodoEntity(null, title, title, false)
        viewModel.insertTask(entity)

        assertThat(repository.tasks).containsExactly(
            TodoEntity(repository.lastIndex, title, title, false)
        )
    }
}