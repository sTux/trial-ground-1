package com.stux.todo.datasource

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.stux.todo.database.TodoEntity
import com.stux.todo.interfaces.ITodoRepository

class TodoRepositoryMock : ITodoRepository {
    private var nextIndex = 0   // Internally holds the index that would be used for next insertion
    val lastIndex   // Public field to access the last index that was inserted
    get() = nextIndex - 1

    val tasks: MutableList<TodoEntity> = mutableListOf()
    private val tasksStore: MutableLiveData<List<TodoEntity>> = MutableLiveData()

    override fun getUnfinishedTasks(): LiveData<List<TodoEntity>> {
        return Transformations.map(tasksStore) { taskList ->
            taskList.filter { entity -> entity.is_complete == false }
        }
    }

    override fun getFinishedTasks(): LiveData<List<TodoEntity>> {
        return Transformations.map(tasksStore) { tasksList ->
            tasksList.filter { entity -> entity.is_complete == true }
        }
    }

    override suspend fun getTask(row: Int): TodoEntity? = tasks.find { it.id == row }

    override suspend fun insert(todoEntity: TodoEntity): Long {
        if (todoEntity.id == null) {
            val newEntity = TodoEntity(
                id = nextIndex,
                subject = todoEntity.subject,
                description = todoEntity.description,
                is_complete = todoEntity.is_complete
            )
            nextIndex++
            tasks.add(newEntity)
        } else tasks.add(todoEntity)

        tasksStore.postValue(tasks)

        return lastIndex.toLong()
    }

    override suspend fun update(todoEntity: TodoEntity) {
        tasks.remove(todoEntity)
        tasks.add(todoEntity)
        tasksStore.postValue(tasks)
    }

    override suspend fun delete(todoEntity: TodoEntity) {
        tasks.remove(todoEntity)
        tasksStore.postValue(tasks)
    }
}