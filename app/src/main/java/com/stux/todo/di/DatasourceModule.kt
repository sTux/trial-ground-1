package com.stux.todo.di

import com.stux.todo.database.TodoDao
import com.stux.todo.datasource.TodoRepository
import com.stux.todo.interfaces.ITodoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object DatasourceModule {
    @Singleton
    @Provides
    fun providesTodoRepository(
        dao: TodoDao
    ): ITodoRepository = TodoRepository(dao)
}