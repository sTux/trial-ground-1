package com.stux.todo.di

import android.content.Context
import androidx.room.Room
import com.stux.todo.database.ApplicationDatabase
import com.stux.todo.database.TodoDao
import com.stux.todo.database.migrations.MIGRATION_1_2
import com.stux.todo.database.migrations.MIGRATION_2_3
import com.stux.todo.utils.Constants.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
object DatabaseModule {
    @Singleton
    @Provides
    fun providesDatabase(
        @ApplicationContext context: Context
    ): ApplicationDatabase = Room.databaseBuilder(
        context,
        ApplicationDatabase::class.java,
        DATABASE_NAME
    ).addMigrations(
        MIGRATION_1_2,
        MIGRATION_2_3
    ).fallbackToDestructiveMigrationOnDowngrade().enableMultiInstanceInvalidation().build()


    @Singleton
    @Provides
    fun providesTodoDao(
        database: ApplicationDatabase
    ): TodoDao = database.todoDao()

}