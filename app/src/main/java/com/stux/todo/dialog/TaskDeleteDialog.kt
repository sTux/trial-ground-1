package com.stux.todo.dialog

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import com.stux.todo.datasource.TodoViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TaskDeleteDialog(private val taskId: Int): DialogFragment() {
    private val viewModel: TodoViewModel by activityViewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let { fragmentActivity ->
            val dialog = AlertDialog.Builder(fragmentActivity)
            dialog.setTitle("Delete?")

            dialog.setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                viewModel.deleteTask(taskId)
            }

            dialog.setNegativeButton(android.R.string.cancel) { d: DialogInterface, _: Int ->
                d.dismiss()
            }

            dialog.create()
        } ?: throw IllegalStateException("Activity is null")
    }
}