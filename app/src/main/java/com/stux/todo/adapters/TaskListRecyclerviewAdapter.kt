package com.stux.todo.adapters

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.RecyclerView
import com.stux.todo.R
import com.stux.todo.database.TodoEntity
import com.stux.todo.interfaces.TaskListHandler
import kotlinx.android.synthetic.main.layout_task_item.view.*
import java.text.SimpleDateFormat


class TaskListRecyclerviewAdapter(
    var tasks: LiveData<List<TodoEntity>>,
    private val clickListener: TaskListHandler
): RecyclerView.Adapter<TaskListRecyclerviewAdapter.TaskViewHolder>() {
    inner class TaskViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val taskTitle: TextView = itemView.taskTitle
        var id: Int? = null
        var isComplete: Boolean = false
        val reminderDate = itemView.taskDueDate

        init {
            itemView.markAsDone.setOnCheckedChangeListener { _, _ ->
                id?.let {  clickListener.clickListener(it) }
            }

            itemView.setOnClickListener {
                id?.let { clickListener.clickListener(it) }
            }

            itemView.setOnLongClickListener {
                if (!isComplete) id?.let { clickListener.clickListenerLong(it) }

                true
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val inflated = LayoutInflater.from(parent.context).inflate(R.layout.layout_task_item, parent, false)

        return TaskViewHolder(inflated)
    }

    override fun getItemCount(): Int = tasks.value?.size?: 0

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        with(holder) {
            tasks.value?.get(position)?.let { task ->
                Log.d(this@TaskListRecyclerviewAdapter.toString(), task.toString())
                taskTitle.text = task.subject
                this.id = task.id
                this.isComplete = task.is_complete ?: false

                if (task.date == null)  this.itemView.taskDueDate.visibility = View.GONE
                else {
                    val date = SimpleDateFormat("dd-MMMM").format(task.date!!)
                    this.reminderDate.text = itemView.context.getString(R.string.due, date)
                }
            }
        }
    }
}