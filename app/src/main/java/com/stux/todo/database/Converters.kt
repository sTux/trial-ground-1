package com.stux.todo.database

import androidx.room.TypeConverter
import java.text.SimpleDateFormat
import java.util.*


class Converters {
    @TypeConverter
    fun fromStringToDate(date: String?): Date? {
        return date?.let {
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            formatter.parse(it)
        }

    }

    @TypeConverter
    fun fromDateToString(date: Date?): String? {
        return date?.let {
            SimpleDateFormat("yyyy-MM-dd").format(it)
        }
    }
}