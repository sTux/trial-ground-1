package com.stux.todo.database.migrations

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase


/**
 * Adds the body column to the "todo" database
 */
val MIGRATION_1_2 = object: Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("""
            ALTER TABLE todo ADD COLUMN body TEXT
        """.trimIndent())
    }
}


/**
 * Adds the date columns to the "todo" database
 */
val MIGRATION_2_3 = object : Migration(2, 3) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("""
            ALTER TABLE todo ADD COLUMN date TEXT DEFAULT NULL
        """.trimIndent())
    }
}