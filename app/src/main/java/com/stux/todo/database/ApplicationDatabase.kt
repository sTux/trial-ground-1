package com.stux.todo.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters


@Database(entities = [TodoEntity::class], version = 3, exportSchema = true)
@TypeConverters(Converters::class)
abstract class ApplicationDatabase: RoomDatabase()  {
    abstract fun todoDao(): TodoDao
}