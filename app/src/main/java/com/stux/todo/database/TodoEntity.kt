package com.stux.todo.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "todo")
data class TodoEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "rowId") val id: Int?,
    @ColumnInfo(name = "subject") var subject: String,
    @ColumnInfo(name = "body") var description: String?,
    @ColumnInfo(name = "completed", defaultValue = "0") var is_complete: Boolean?,
    @ColumnInfo(name = "date") var date: Date? = null
)
