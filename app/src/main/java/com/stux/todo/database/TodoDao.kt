package com.stux.todo.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update


@Dao
interface TodoDao {
    @Query("SELECT * FROM todo WHERE completed = :status")
    fun getTasks(status: Int): LiveData<List<TodoEntity>>

    @Query("SELECT * FROM todo WHERE rowId = :row")
    suspend fun getTask(row: Int): TodoEntity

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(todoEntity: TodoEntity): Long

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun update(todoEntity: TodoEntity)

    @Delete()
    suspend fun delete(todoEntity: TodoEntity)
}