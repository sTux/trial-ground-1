package com.stux.todo.datasource

import androidx.lifecycle.LiveData
import com.stux.todo.database.TodoDao
import com.stux.todo.database.TodoEntity
import com.stux.todo.interfaces.ITodoRepository


class TodoRepository(private val todoDao: TodoDao): ITodoRepository {
    override fun getUnfinishedTasks(): LiveData<List<TodoEntity>> = todoDao.getTasks(status = 0)
    override fun getFinishedTasks(): LiveData<List<TodoEntity>> = todoDao.getTasks(status = 1)
    override suspend fun getTask(row: Int): TodoEntity = todoDao.getTask(row)

    override suspend fun  insert(todoEntity: TodoEntity): Long = todoDao.insert(todoEntity)
    override suspend fun update(todoEntity: TodoEntity) = todoDao.update(todoEntity)
    override suspend fun delete(todoEntity: TodoEntity) = todoDao.delete(todoEntity)
}