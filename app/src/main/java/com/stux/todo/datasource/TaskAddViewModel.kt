package com.stux.todo.datasource

import android.annotation.SuppressLint
import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.stux.todo.database.TodoEntity
import com.stux.todo.interfaces.ITodoRepository
import com.stux.todo.utils.Constants.MAXIMUM_TITLE_LENGTH
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
class TaskAddViewModel @ViewModelInject constructor(private val repository: ITodoRepository) : ViewModel() {
    val status = MutableLiveData<Long>()
    private var _reminderDate: String? = null
    var reminderDate: Date?
        get() {
            _reminderDate?.let {
                return SimpleDateFormat("yyyy-MM-dd").parse(it)
            }
            return null
        }
        set(value) {
            SimpleDateFormat("yyyy-MM-dd").also {
                value?.let { date -> _reminderDate = it.format(date) }
            }
        }
    private var _reminderTime: String? = null
    var reminderTime: Date?
        get() {
            _reminderTime?.let {
                return SimpleDateFormat("HH:mm").parse(it)
            }
            return null
        }
        set(value) {
            SimpleDateFormat("HH:mm").also {
                value?.let { time -> _reminderTime = it.format(time) }
            }
        }

    fun validateTasks(task: TodoEntity): Boolean =
        !(task.subject.isBlank() || task.subject.length > MAXIMUM_TITLE_LENGTH)

    fun insertTask(task: TodoEntity) = viewModelScope.launch {
        /**
         * Launch a co-routine to insert the TaskEntity and save it in the database
         * The insert() is a suspend function and thus this operation happens on a background thread
         */
        if (validateTasks(task)) {
            val taskId = repository.insert(todoEntity = task)
            Log.i(this@TaskAddViewModel.javaClass.canonicalName, "Inserted the task $taskId")
            status.postValue(taskId)
        } else {
            Log.i(this@TaskAddViewModel.javaClass.canonicalName, "Task data is invalid")
            status.postValue(-1)
        }
    }
}