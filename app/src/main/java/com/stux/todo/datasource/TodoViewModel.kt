package com.stux.todo.datasource

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.stux.todo.database.TodoEntity
import com.stux.todo.interfaces.ITodoRepository
import kotlinx.coroutines.launch

class TodoViewModel @ViewModelInject constructor(private val todoRepository: ITodoRepository): ViewModel() {

    val unfinishedTasks: LiveData<List<TodoEntity>> = todoRepository.getUnfinishedTasks()
    val finishedTasks: LiveData<List<TodoEntity>> = todoRepository.getFinishedTasks()

    fun markFinished(task: Int) = viewModelScope.launch {
        /**
         * Launch a co-routine to fetch task from the database, mark it as complete and update the db
         * Do it in the background so that UI thread doesn't get stuck.
         * The `getTask()` method in Dao and Repository are marked as suspend because Room doesn't allow running queries in UI Thread.
         * Thus marking them as suspending function, ensures that when they're evoked from this co-routine, the run on background thread
         */
        val entity = todoRepository.getTask(task)
        entity?.let {
            it.is_complete = true
            todoRepository.update(it)
            Log.i(this@TodoViewModel.toString(), "Marked task ${entity.subject} as complete")
        } ?: Log.e(this@TodoViewModel.toString(), "Invalid tasks requested")
    }

    fun deleteTask(task: Int) = viewModelScope.launch {
        /**
         * Launch a co-routine to fetch the task from database and delete it
         * The delete() is a suspend function and thus this operation happens on a background thread
         */
        val entity = todoRepository.getTask(task)
        entity?.let {
            todoRepository.delete(entity)
            Log.i(this@TodoViewModel.toString(), "Deleted task ${entity.subject}")
        } ?: Log.e(this@TodoViewModel.toString(), "Invalid tasks requested")
    }
}