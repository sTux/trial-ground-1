package com.stux.todo.utils

object Constants {
    const val MAXIMUM_TITLE_LENGTH: Int = 100
    const val DATABASE_NAME = "todo_db"
}