package com.stux.todo.utils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment


fun Fragment.makeToast(message: String) {
    val toast = Toast.makeText(
        this.context,
        message,
        Toast.LENGTH_LONG
    )
    toast.show()
}

fun AppCompatActivity.makeToast(message: String) {
    val toast = Toast.makeText(
        this,
        message,
        Toast.LENGTH_LONG
    )
    toast.show()
}