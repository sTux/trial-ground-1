package com.stux.todo.screens

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.stux.todo.R
import com.stux.todo.database.TodoEntity
import com.stux.todo.databinding.FragmentAddTaskBinding
import com.stux.todo.datasource.TaskAddViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class AddTaskFragment : Fragment() {
    private lateinit var binding: FragmentAddTaskBinding
    private val viewModel: TaskAddViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_add_task,
            container,
            false
        )

        binding.chipDateGroup.setOnCheckedChangeListener { _, checkedId ->
            val calendar = Calendar.getInstance()
            when (checkedId) {
                R.id.setDateToday -> viewModel.reminderDate = calendar.time
                R.id.setDateTomorrow -> {
                    calendar.roll(Calendar.DATE, true)
                    viewModel.reminderDate = calendar.time
                }
                R.id.setDateCustom -> Log.wtf(this.tag, "Not Implemented!")
            }

            Log.i(this.javaClass.canonicalName, "Set reminder date ${viewModel.reminderDate}")
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_add_task, menu)
    }

    private fun insertTask() {
        val task = TodoEntity(
            id = null,
            subject = binding.taskTitleInput.text.toString(),
            description = binding.taskBodyInput.text.toString(),
            is_complete = false,
            date = viewModel.reminderDate
        )

        viewModel.insertTask(task)

        if (viewModel.status.value == -1L) {
            Toast.makeText(context, "Title not valid. Not inserting", Toast.LENGTH_LONG).show()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.taskAddButton -> {
                this.insertTask()
                findNavController().navigateUp()
            }
            R.id.taskDiscardButton, android.R.id.home -> findNavController().navigateUp()
            else -> super.onOptionsItemSelected(item)
        }
    }
}