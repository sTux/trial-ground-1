package com.stux.todo.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.stux.todo.R
import com.stux.todo.adapters.TaskListRecyclerviewAdapter
import com.stux.todo.databinding.FragmentTaskDisplayBinding
import com.stux.todo.datasource.TodoViewModel
import com.stux.todo.dialog.TaskDeleteDialog
import com.stux.todo.interfaces.TaskListHandler
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.util.Calendar

@AndroidEntryPoint
class TaskDisplayFragment : Fragment() {
    private lateinit var binding: FragmentTaskDisplayBinding
    private val viewModel: TodoViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    @Suppress("DEPRECATION")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_task_display,
            container,
            false
        )
        binding.floatingActionButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.action_tasksFragment_to_addTaskFragment)
        )

        val formatter = SimpleDateFormat("EE, MMM dd", resources.configuration.locale)
        binding.todaysDate.text = formatter.format(Calendar.getInstance().time)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.taskListView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = TaskListRecyclerviewAdapter(viewModel.unfinishedTasks, taskListHandler)
        }

        viewModel.unfinishedTasks.observe(viewLifecycleOwner, Observer {
            binding.taskListView.adapter?.notifyDataSetChanged()
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.menu_toolbar, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.showFinished -> {
                binding.taskListView.adapter?.let {
                    taskSwitchHandler(it as TaskListRecyclerviewAdapter, item)
                }
                item.isChecked = item.isChecked.not()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun taskSwitchHandler(adapter: TaskListRecyclerviewAdapter, check: MenuItem) {
        /**
         * Handles the switching of  TaskListRecyclerviewAdapter data-set change.
         * The change has main steps :-
         *      First remove the observers of the currently referenced LiveData as those observers may trigger unwanted `.notifyDataSetChanged()`
         *      Change the actual LiveData reference in the adapter
         *      Add the new Observer since without the observer the adapters won't update
         *      Change the menu item to reflect the appropriate title
         */
        if (check.isChecked) {
            viewModel.finishedTasks.removeObservers(viewLifecycleOwner)
            adapter.tasks = viewModel.unfinishedTasks
            viewModel.unfinishedTasks.observe(viewLifecycleOwner, Observer { adapter.notifyDataSetChanged() })
            check.setTitle(R.string.show_finished_tasks)
        } else {
            viewModel.unfinishedTasks.removeObservers(viewLifecycleOwner)
            adapter.tasks = viewModel.finishedTasks
            viewModel.finishedTasks.observe(viewLifecycleOwner, Observer { adapter.notifyDataSetChanged() })
            check.setTitle(R.string.show_unfinished_tasks)
        }
    }

    private val taskListHandler = object : TaskListHandler {
        override fun clickListener(task: Int) {
            Toast.makeText(
                context,
                "Will show the details for task $task",
                Toast.LENGTH_LONG
            ).show()
        }

        override fun clickListenerLong(task: Int) {
            val dialog = TaskDeleteDialog(taskId = task)
            activity?.let { dialog.show(it.supportFragmentManager, "deleteTask") }
        }
    }
}