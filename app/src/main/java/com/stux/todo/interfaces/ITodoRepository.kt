package com.stux.todo.interfaces

import androidx.lifecycle.LiveData
import com.stux.todo.database.TodoEntity


interface ITodoRepository {
    fun getUnfinishedTasks(): LiveData<List<TodoEntity>>
    fun getFinishedTasks(): LiveData<List<TodoEntity>>
    suspend fun getTask(row: Int): TodoEntity?

    suspend fun insert(todoEntity: TodoEntity): Long
    suspend fun update(todoEntity: TodoEntity)
    suspend fun delete(todoEntity: TodoEntity)
}