package com.stux.todo.interfaces

interface TaskListHandler {
    fun clickListener(task: Int)
    fun clickListenerLong(task: Int)
}