package com.stux.todo.database

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertWithMessage
import com.stux.todo.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
@SmallTest
class TaskDatabaseTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private lateinit var context: Context
    private lateinit var database: ApplicationDatabase
    private lateinit var taskDao: TodoDao

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, ApplicationDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        taskDao = database.todoDao()

        runBlockingTest {
            val task1 = TodoEntity(id =1, subject = "Hello World!", description = "Task Description!", is_complete = false)
            taskDao.insert(task1)
            val task2 = TodoEntity(id = 2, subject = "Another Task!", description = "Task Description!", is_complete = true)
            taskDao.insert(task2)
            val task3 = TodoEntity(id = 3, subject = "Final Task!", description = "Task Description!", is_complete = false)
            taskDao.insert(task3)
        }
    }

    @Test
    fun testTaskSelect() = runBlockingTest {
        val task = TodoEntity(id = 2, subject = "Another Task!", description = "Task Description!", is_complete = true)
        val dbResult = taskDao.getTask(2)

        assertWithMessage("Entity added properly").that(dbResult).isNotNull()
        assertWithMessage("Task added is same as created").that(dbResult).isEqualTo(task)
    }

    @Test
    fun testTasksSelect() = runBlockingTest {
        val tasks = taskDao.getTasks(0).getOrAwaitValue()

        assertWithMessage("Returns the same number of tasks as expected").that(tasks).hasSize(2)
    }


    @Test
    fun taskInsertionCorrect() = runBlockingTest {
        val dbResult = taskDao.getTasks(0).getOrAwaitValue()

        assertWithMessage("Task insertions are correct").that(dbResult).containsExactly(
            TodoEntity(id =1, subject = "Hello World!", description = "Task Description!", is_complete = false),
            TodoEntity(id = 3, subject = "Final Task!", description = "Task Description!", is_complete = false)
        )
    }

    @Test
    fun testIgnoresInsertionOnConflict() = runBlockingTest {
        val task = TodoEntity(1, "Another Task!", "Task Description!", false)
        taskDao.insert(task)
        val result = taskDao.getTask(1)

        assertWithMessage("Conflicting Entity insertion ignored")
            .that(result.subject).doesNotContain("Another Task!")

        assertWithMessage("Existing Entity remains unchanged")
            .that(result.subject).isEqualTo("Hello World!")
    }

    @Test
    fun testDelete() = runBlockingTest {
        val task = TodoEntity(id = 3, subject = "Final Task!", description = "Task Description!", is_complete = false)
        taskDao.delete(task)

        val tasks = taskDao.getTasks(0).getOrAwaitValue()
        assertWithMessage("Deleted task not present in result").that(tasks).doesNotContain(task)
    }

    @Test
    fun testTaskUpdate() = runBlockingTest {
        val task = TodoEntity(id = 2, subject = "Updated Task!", description = "Task Description!", is_complete = false)
        taskDao.update(task)
        val dbResult = taskDao.getTasks(0).getOrAwaitValue()

        assertWithMessage("Task Updated successfully").that(dbResult).contains(task)
    }

    @After
    fun teardown() {
        database.close()
    }
}