package com.stux.todo.database

import androidx.core.database.getIntOrNull
import androidx.core.database.getStringOrNull
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import androidx.test.platform.app.InstrumentationRegistry
import com.google.common.truth.Truth.assertThat
import com.stux.todo.database.migrations.MIGRATION_1_2
import com.stux.todo.database.migrations.MIGRATION_2_3
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
@SmallTest
class DatabaseMigrationsTest {
    private val DB_NAME = "testing-db"

    // Helper rule to help in migration testing
    @get:Rule
    val migrationTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        ApplicationDatabase::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    @Test
    fun testMigration1To2() {
        // First create the database using the older version 1 schema, then insert testing data and finally close the database
        migrationTestHelper.createDatabase(DB_NAME, 1).apply {
            execSQL("INSERT INTO todo VALUES (1, 'Sample Task!', 0)")
            close()
        }

        // Now that the database is created with the older schema, use the helper and migrate the db
        val db = migrationTestHelper.runMigrationsAndValidate(
            DB_NAME,
            2,
            true,
            MIGRATION_1_2
        )

        // With the migrations done, execute a query to fetch the inserted sample data
        val resultCursor = db.query("SELECT * FROM todo WHERE rowId = 1")
        resultCursor.moveToFirst()

        // Get the value of the new column to see if it's properly added and get the data
        val descriptionColumnIndex = resultCursor.getColumnIndex("body")
        val descriptionData = resultCursor.getIntOrNull(descriptionColumnIndex)

        // Since the column was present when the data was inserted, so it should be NULL. Check it.
        assertThat(descriptionData).isEqualTo(null)
    }

    @Test
    fun testMigration2To3() {
        migrationTestHelper.createDatabase(DB_NAME, 2).apply {
            execSQL("INSERT INTO todo VALUES (1, 'Sample Task!', 'Description', 0)")
            close()
        }

        // Now that the database is created with the older schema, use the helper and migrate the db
        val db = migrationTestHelper.runMigrationsAndValidate(
            DB_NAME,
            3,
            true,
            MIGRATION_2_3
        )

        // With the migrations done, execute a query to fetch the inserted sample data
        val resultCursor = db.query("SELECT * FROM todo WHERE rowId = 1")
        resultCursor.moveToFirst()

        // Get the value of the new column to see if it's properly added and get the data
        val dateColumnIndex = resultCursor.getColumnIndex("date")
        val dateData = resultCursor.getStringOrNull(dateColumnIndex)

        // Since the column was present when the data was inserted, so it should be NULL. Check it.
        assertThat(dateData).isEqualTo(null)
    }
}