package com.stux.todo.datasource

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.google.common.truth.Truth.assertWithMessage
import com.stux.todo.database.ApplicationDatabase
import com.stux.todo.database.TodoDao
import com.stux.todo.database.TodoEntity
import com.stux.todo.getOrAwaitValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@SmallTest
@ExperimentalCoroutinesApi
class TodoRepositoryTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: ApplicationDatabase
    private lateinit var dao: TodoDao
    private lateinit var repository: TodoRepository
    private lateinit var context: Context


    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        database = Room.inMemoryDatabaseBuilder(context, ApplicationDatabase::class.java)
            .allowMainThreadQueries()
            .build()
        dao = database.todoDao()
        repository = TodoRepository(dao)

        runBlockingTest {
            val task1 = TodoEntity(id = 1, subject = "Hello World!", description = "Task Description!", is_complete = false)
            repository.insert(task1)
            val task2 = TodoEntity(id = 2, subject = "Another Task!", description = "Task Description!", is_complete = true)
            repository.insert(task2)
            val task3 = TodoEntity(id = 3, subject = "Final Task!", description = "Task Description!", is_complete = false)
            repository.insert(task3)
        }
    }

    @After
    fun teardown() {
        database.close()
    }


    @Test
    fun testTaskSelect() = runBlockingTest {
        val task = TodoEntity(id = 2, subject = "Another Task!", description = "Task Description!", is_complete = true)
        val dbResult = repository.getTask(2)

        assertWithMessage("Entity added properly").that(dbResult).isNotNull()
        assertWithMessage("Task added is same as created").that(dbResult).isEqualTo(task)
    }

    @Test
    fun testGetUnfinishedTasks() {
        val tasks = repository.getUnfinishedTasks().getOrAwaitValue()

        assertWithMessage("Repository returns unfinished tasks correctly").that(tasks)
            .containsExactly(
                TodoEntity(id = 1, subject = "Hello World!", description = "Task Description!", is_complete = false),
                TodoEntity(id = 3, subject = "Final Task!", description = "Task Description!", is_complete = false)
            )
    }

    @Test
    fun testGetFinishedTasks() {
        val tasks = repository.getFinishedTasks().getOrAwaitValue()

        assertWithMessage("Repository returns finished tasks correctly").that(tasks)
            .containsExactly(
                TodoEntity(id = 2, subject = "Another Task!", description = "Task Description!", is_complete = true)
            )
    }

    @Test
    fun testDelete() = runBlockingTest {
        val task = TodoEntity(id = 3, subject = "Final Task!", description = "Task Description!", is_complete = false)
        repository.delete(task)

        val tasks = dao.getTasks(0).getOrAwaitValue()
        assertWithMessage("Deleted task not present in result").that(tasks)
            .doesNotContain(task)
    }

    @Test
    fun testTaskUpdate() = runBlockingTest {
        val task = TodoEntity(id = 2, subject = "Updated Task!", description = "Task Description!", is_complete = false)
        repository.update(task)
        val dbResult = dao.getTasks(0).getOrAwaitValue()

        assertWithMessage("Task Updated successfully").that(dbResult).contains(task)
    }
}